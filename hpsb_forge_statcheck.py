import requests
import json
from datetime import datetime


with open("config.json", "rt") as f:
    USERNAME = json.load(f)["username"]

PROFILE_API_ENDPOINT = "https://sky.shiiyu.moe/api/v2/profile/{name}"


def fail(msg: str) -> None:
    print(msg)
    exit()


if __name__ == "__main__":
    try:
        profile_lookup_response = requests.get(PROFILE_API_ENDPOINT.format(name=USERNAME))
    except requests.exceptions.ConnectionError:
        fail("profile lookup failed - API unresponsive")
    if not str(profile_lookup_response.status_code).startswith("2"):
        fail("profile lookup failed - API unresponsive")

    profile_lookup_data = profile_lookup_response.json()
    profiles = profile_lookup_data.get("profiles", [])
    profile = None
    for prof in profiles:
        if profiles[prof]["current"]:
            profile = profiles[prof]
            break
    if profile is None:
        fail("No active profile")

    data = profile["data"]["mining"]["forge"].get("processes", [])
    parsed = [[i["name"], i["slot"], i["timeFinished"]] for i in data]
    if len(parsed) == 0:
        fail("No forge processes found.")
    for name, slot, tf, in parsed:
        print(datetime.fromtimestamp(tf / 1000).strftime(
            f"{name} on slot {slot} - ends %H:%M:%S %d.%m.%Y"
        ))
