import requests
import json
import yaml
import math
from datetime import datetime


try:
    f = open("config.yml", "rt")
    CONFIG = yaml.safe_load(f)
except FileNotFoundError:
    try:
        f = open("config.yaml", "rt")
        CONFIG = yaml.safe_load(f)
    except FileNotFoundError:
        try:
            f = open("config.json", "rt")
            CONFIG = json.load(f)
        except FileNotFoundError:
            print("Connot read config file!")
            exit()

f.close()

AUTO_QF_LEVEL_LOOKUP = CONFIG["auto_quick_forge_level_lookup"]
AUTO_FORGE_SLOTS_LOOKUP = CONFIG["auto_forge_slots_lookup"]
BACKUP_FORGE_SLOTS = CONFIG["backup_forge_slots"]
BACKUP_QUICK_FORGE_LEVEL = CONFIG["backup_quick_forge_level"]
TO_LEAVE = CONFIG["to_leave"]

USERNAME = CONFIG["username"]

BAZAAR_API_ENDPOINT = "https://api.hypixel.net/v2/skyblock/bazaar"
PROFILE_API_ENDPOINT = "https://sky.shiiyu.moe/api/v2/profile/{name}"

NAMES = [
    "GEMSTONE_MIXTURE",
    "SLUDGE_JUICE",
    "FINE_JADE_GEM",
    "FINE_AMBER_GEM",
    "FINE_SAPPHIRE_GEM",
    "FINE_AMETHYST_GEM"
]


def fail(msg: str) -> None:
    print(msg)
    exit()


def get_hotm_data() -> dict:
    profile_lookup_response = requests.get(PROFILE_API_ENDPOINT.format(name=USERNAME))
    if not str(profile_lookup_response.status_code).startswith("2"):
        fail("profile lookup failed - API unresponsive")
    profile_lookup_data = profile_lookup_response.json()
    profiles = profile_lookup_data.get("profiles", [])
    profile = None
    for prof in profiles:
        if profiles[prof]["current"]:
            profile = profiles[prof]
            break
    if profile is None:
        fail("No active profile")
    mining_data = profile["data"]["mining"]
    hotm_data = mining_data.get("core", None)
    if hotm_data is None:
        fail("HOTM quest incomplete")
    return hotm_data


def get_forge_slots(hotm_data: dict) -> int:
    slots = 2
    if hotm_data["level"]["level"] >= 3:
        slots += 1
    if hotm_data["level"]["level"] >= 4:
        slots += 1
    potm = hotm_data["nodes"].get("special_0", 0)
    if potm >= 2:
        slots += 1
    return slots


def time(qf_level: int, /) -> int:
    default = 14400
    bonus = round(qf_level / 200, 3)
    if qf_level > 0:
        bonus += .1
    return math.ceil(default * (1 - bonus))


def format_number(price: float, /) -> str:
    sign = "- " if price < 0 else ""
    c, p = list(str(abs(int(price)))), []
    for i in c[::-1]:
        if len(p) == 0:
            p.append(i)
        elif len(p[0]) < 3:
            p[0] = i + p[0]
        else:
            p.insert(0, i)
    return sign + " ".join(p)


class Mode:
    def __init__(self, mats: float, gm_price: float, qf_level: int, forge_slots: int, comment: str, /) -> None:
        self.comment = comment
        self.gross_per_batch = (forge_slots - TO_LEAVE) * gm_price * .9875
        self.profit_per_batch = self.gross_per_batch - forge_slots * mats
        self.gross_per_second = self.gross_per_batch / time(qf_level)
        self.profit_per_second = self.profit_per_batch / time(qf_level)
        self.gross_per_hour = self.gross_per_second * 3600
        self.profit_per_hour = self.profit_per_second * 3600
        self.gross_per_day = self.gross_per_hour * 24
        self.profit_per_day = self.profit_per_hour * 24
    
    def print(self) -> None:
        print("-" * 53)
        print(f"\tif you {self.comment}:".upper())
        print("\tprofit per batch:", format_number(self.profit_per_batch), sep="\t")
        print("\tprofit per hour:", format_number(self.profit_per_hour), sep="\t")
        print("\tprofit per day:", format_number(self.profit_per_day), sep="\t\t")
        print("")


if __name__ == "__main__":
    if AUTO_QF_LEVEL_LOOKUP or AUTO_FORGE_SLOTS_LOOKUP:
        hotm_data = get_hotm_data()
        if AUTO_QF_LEVEL_LOOKUP:
            qf_level = hotm_data["nodes"].get("forge_time", 0)
        else:
            qf_level = BACKUP_QUICK_FORGE_LEVEL
        if AUTO_FORGE_SLOTS_LOOKUP:
            forge_slots = get_forge_slots(hotm_data)
        else:
            forge_slots = BACKUP_FORGE_SLOTS
    else:
        qf_level, forge_slots = BACKUP_QUICK_FORGE_LEVEL, BACKUP_FORGE_SLOTS

    response = requests.get(BAZAAR_API_ENDPOINT)
    if not str(response.status_code).startswith("2"):
        print("API ERROR")
        exit()
    data = response.json()
    if not data.get("success", False):
        print("API ERROR")
        exit()
    products = data.get("products", None)
    if products is None:
        print("API ERROR")
        exit()
    updtime = data.get("lastUpdated", None)
    if updtime is not None:
        print(datetime.fromtimestamp(updtime / 1000).strftime("Last update: %H:%M:%S %d.%m.%Y\n"))
    orders = [[products[i]["buy_summary"], products[i]["sell_summary"]] for i in NAMES]
    prices = []
    for i in range(len(orders)):
        orders[i][0].sort(key=lambda e: e["pricePerUnit"])
        orders[i][1].sort(reverse=True, key=lambda e: e["pricePerUnit"])
        prices.append([orders[i][0][0]["pricePerUnit"], orders[i][1][0]["pricePerUnit"]])
    sludge_price, gems_price = prices[1][0] * 320, sum([prices[i][0] for i in range(2, 6)]) * 4

    modes = [
        Mode(sludge_price + gems_price, prices[0][1], qf_level, forge_slots, "buy everything"),
        Mode(sludge_price, prices[0][1], qf_level, forge_slots, "buy sludge and mine gemstones"),
        Mode(gems_price, prices[0][1], qf_level, forge_slots, "buy gemstones and mine sludge"),
        Mode(0, prices[0][1], qf_level, forge_slots, "mine everything")
    ]
    
    for i in modes:
        i.print()
