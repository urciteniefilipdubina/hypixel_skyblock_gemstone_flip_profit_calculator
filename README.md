# Credits to [SkyCrypt API](https://sky.shiiyu.moe/api) (This thing uses it).
# How to run?
# SETUP:
## Step 1 - clone the repository
## Step 2 - open a terminal in the local version of the repository
## Step 3 - install needed libraries
```shell
python3 -m pip install -r requirements.txt
```
## Step 4 - create a `config.yml` or `config.yaml` or `config.json` file with following fields:
* `"auto_quick_forge_level_lookup"` -> true/false
* `"auto_forge_slots_lookup"` -> true/false
* `"username"` -> your minecraft username
* `"backup_quick_forge_level"` -> number (in case automatic lookup fails / is turned off)
* `"backup_forge_slots"` -> number (in case automatic lookup fails / is turned off)
* `"to_leave"` -> number (how many of the forged GMs do you want to keep instead of selling them)
# RUNNING:
## Flip profit checker:
```shell
python3 hpsb_gm_flip.py
```
## Forge status checker:
```shell
python3 hpsb_forge_statcheck.py
```

