from datetime import datetime
import io
import time
import threading


EPOCH = 1560275700
MONTH_NAMES = [
    "Early Spring,",
    "Spring,",
    "Late Spring,",
    "Early Summer,",
    "Summer,",
    "Late Summer,",
    "Early Autumn,",
    "Autumn,",
    "Late Autumn,",
    "Early Winter,",
    "Winter,",
    "Late Winter,"
]


def dform(d: int) -> str:
    if d in [1, 21, 31]:
        suff = "st"
    elif d in [2, 22]:
        suff = "nd"
    elif d in [3, 23]:
        suff =  "rd"
    else:
        suff = "th"
    return str(d) + suff


def hform(h: int, m: int) -> str:
    disph = h % 12
    t = h // 12
    if disph == 0:
        disph = 12
    suff = "pm" if t else "am"
    sm = str(m)
    if len(sm) == 1:
        sm = "0" + sm
    return str(disph) + ":" + sm + suff


def get_ds() -> str:
    dur = datetime.now().timestamp() - EPOCH
    year = int((dur // 446400) + 1)
    _y_left = dur % 446400
    month = int(_y_left // 37200)
    _mo_left = _y_left % 37200
    day = int((_mo_left // 1200) + 1)
    _d_left = _mo_left % 1200
    hour = int(_d_left // 50)
    _h_left = _d_left % 50
    tm = int(_h_left / (5 / 6))
    with io.StringIO() as r:
        print(
            hform(hour, tm), "-", dform(day), "of", MONTH_NAMES[month], "year", year,
            end="", file=r, flush=True
        )
        r.seek(0)
        s = r.read()
    return s


def ilprint(*args, **kwargs) -> int:
    lw = kwargs.pop("lw", 0)
    if not isinstance(lw, int):
        raise ValueError("lw must be int!")
    try:
        kwargs.pop("file")
    except KeyError:
        pass
    try:
        kwargs.pop("flush")
    except KeyError:
        pass
    try:
        kwargs.pop("end")
    except KeyError:
        pass
    print("\b" * lw, end="", flush=True)
    with io.StringIO() as r:
        print(*args, **kwargs, end="", flush=True, file=r)
        r.seek(0)
        s = r.read()
    print(s, end="", flush=True)
    return len(s)


class Processor(threading.Thread):
    def __init__(self, delay: float, /) -> None:
        super(Processor, self).__init__()
        self.running = False
        self.delay = delay
        self.lw = 0
    
    def run(self) -> None:
        self.running = True
        while self.running:
            self.lw = ilprint(get_ds(), lw=self.lw)
            time.sleep(self.delay)

if __name__ == "__main__":
    try:
        p = Processor(5 / 12)
        p.start()
        input()
        exit(0)
    except BaseException as e:
        p.running = False
        del p
        print("")
        raise e
